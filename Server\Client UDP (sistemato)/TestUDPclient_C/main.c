

                //UDP_CLIENT


#include <stdio.h>
#include <stdlib.h>
#include <string.h>       //memset
#include <sys/types.h>     //header files for reading socket API
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define SERVER "127.0.0.1"            
#define SERVPORT 3000          //The port on which to send data

#define MSG "Messaggio di richesta da parte del client"


int main(int argc, char *argv[])
{
    int sd;                                       
    struct sockaddr_in serveraddr, clientaddr;
    int serveraddrlen = sizeof(serveraddr);
    char server[255];
    char buffer[100];
    char *bufptr = buffer;
    int buflen = sizeof(buffer);
    struct hostent *hostp;

    memset(buffer, 0x00, sizeof(buffer));            
    strcpy(buffer, MSG);     
	                                                             
    printf("La lunghezza del messaggio e':%d\n",strlen(buffer));


    //Socket

    if((sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
	perror("UDP Client - ERROR socket");
    }
	    else
		printf("UDP Client - socket OK\n");

    if(argc > 1)                                       //If we haven't server IP/hostname
    {
	strcpy(server, argv[1]);
    }                           
	    else
	    {
		printf("UDP Client - %s <Server hostname o IP>\n", argv[0]);   
		printf("UDP Client - Viene usato hostname/IP di default!\n");

		strcpy(server, SERVER);
	    }

    memset(&serveraddr, 0x00, sizeof(struct sockaddr_in));       
    serveraddr.sin_family = AF_INET;                             
    serveraddr.sin_port = htons(SERVPORT);                       //htons()----host to network short

    if (inet_aton(SERVER , &serveraddr.sin_addr) == 0)           //convert SERVER to address stored in serveraddr
    {
	fprintf(stderr, "inet_aton() fallita\n");
	exit(1);
    }


    //Send the message to server 

   if((sendto(sd, buffer, strlen(buffer), 0, (struct sockaddr *)&serveraddr, sizeof(serveraddr))) == -1)
    {
	perror("UDP Client - sendto() ERROR");
	close(sd);
    }
	    else
		printf("UDP Client - sendto() OK!\n");

    printf("Attesa risposta dal server UDP...\n");


    //Receive a reply and print it in buffer

    if((recvfrom(sd, buffer, 100, 0, (struct sockaddr *)&serveraddr, &serveraddrlen)) == -1)    
    {
	perror("UDP Client - recvfrom() ERROR");
	close(sd);
    }
	    else
	    {
		printf("UDP client ha ricevuto il seguente messaggio di risposta dal server: \"%s\"\n", bufptr);
		printf("dalla porta %d, indirizzo %s\n", ntohs(serveraddr.sin_port), inet_ntoa(serveraddr.sin_addr));
	    }

    close(sd);
    exit(0);
}
