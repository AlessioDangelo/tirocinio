

                //UDP_SERVER

#include <unistd.h>
#include <stdio.h>            
#include <stdlib.h>
#include <string.h>       //memset
#include <sys/types.h>     //header files for reading socket API   
#include <sys/socket.h>       
#include <netinet/in.h>       
#include <arpa/inet.h>

#define SERVPORT 3000       //The port on which to listen for incoming data 
#define SERVER "127.0.0.1"


int main(int argc, char *argv[])
{
	int sd;                                  
	struct sockaddr_in serveraddr, clientaddr;
	int clientaddrlen = sizeof(clientaddr);
	int serveraddrlen = sizeof(serveraddr);
	char buffer[100];
	char *bufptr = buffer;
	int buflen = sizeof(buffer);
	char server[255];


	//create UDP socket 
                                                               //IPPROTO_UDP=protocol used
	if((sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) ==-1)   //AFI_INET=socket's domain,IP address (host)/port number  
	{                                                          //SOCK_DGRAM=line for data transport,UDP protocol
	perror("UDP server - socket ERROR");
	}
		else
		printf("UDP server - socket OK\n");

	printf("UDP server - tentativo di bind con la porta predefinita... \n");


	//Bind socket to port

	memset(&serveraddr, 0x00, serveraddrlen);           //memset()=set servaddr in hexadecimal
	serveraddr.sin_family = AF_INET;                    
	serveraddr.sin_port = htons(SERVPORT);              
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);     //s_addr=0, so that to allow the system connecting to any clients
	strcpy(server, SERVER);
		                                            

	if((bind(sd, (struct sockaddr *)&serveraddr, serveraddrlen)) == -1)
	{
	perror("UDP server - bind ERROR");
	close(sd);
	}
		else
		printf("UDP server - bind OK\n");

	printf("IP usato %s ,porta server impostata %d\n", inet_ntoa(serveraddr.sin_addr), SERVPORT);
	printf("UDP server - in ascolto...\n");            //keep listening for data


	//Try to receive some data
	//recvform() can attend data by the client for indefinite

	buflen = 100;
	if((recvfrom(sd, buffer, buflen, 0, (struct sockaddr *)&clientaddr, &clientaddrlen)) == -1)
	{
	perror("UDP Server - ERRORE ricevimento dati");
	close(sd);
	}
		else
		printf("UDP Server - dati ricevuti...\n");

	printf("UDP Server ha ricevuto il seguente messaggio: \"%s\" \n", bufptr);
	printf("dalla porta %d e indirizzo %s.\n", ntohs(clientaddr.sin_port),
	inet_ntoa(clientaddr.sin_addr));


	//Reply the client

	if((sendto(sd, buffer, buflen, 0, (struct sockaddr *)&clientaddr, clientaddrlen)) == -1)
	{
	perror("UDP server - ERRORE di risposta");
	close(sd);
	}
		else
		printf("UDP Server - risposta effettuata...\n");

	close(sd);
	exit(0);
}
