
/********UDP server********/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>        //memset
#include <sys/types.h>     //header files per leggere le API socket
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#define CHANNEL_ID 00
#define THRESH1_ID 02
#define THRESH2_ID 03
#define PIXELD_ID 04
#define PIXELE_ID 05
#define THCAN_ID 06
#define VALUE1_ID 07
#define VALUE2_ID 08
#define LENGTH_ID 09
#define GAP_ID 10
#define ANGLE_ID 11

#define SERVPORT 3000             //Porta in ascolto su cui arrivano i dati
#define SERVER "127.0.0.1"

extern pthread_mutex_t lock;

extern int channel;
extern int thresh1;
extern int thresh2;
extern int pixeld;
extern int pixele;
extern int thcan;
extern int value1;
extern int value2;
extern int length;
extern int gap;
extern int angle;

extern char buffer[100];

void *UDPserver_function( void *srv )
{

    int sd;
    struct sockaddr_in serveraddr, clientaddr;
    socklen_t clientaddrlen;
    //int clientaddrlen = sizeof(clientaddr);
    int serveraddrlen = sizeof(serveraddr);
    //
    buffer[0]=THRESH1_ID;                   //parametri per lo switch (temporanei)
    buffer[1]=100;
    //
    char *bufptr = buffer;
    bufptr = (char *) srv;
    int buflen = sizeof(buffer);
    char server[255];
    int end = 0;
    int end_ext = 0;


    while(end==0 && end_ext==0)
    {

        //create UDP socket

                                                                   //IPPROTO_UDP=protocol used
        if((sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) ==-1)   //AFI_INET=dominio della nostra socket,IP address (host)/port number
        {                                                          //SOCK_DGRAM=linea di trasporto dati,UDP protocol
            perror("UDP server - socket ERROR");
        }
        else
            printf("UDP server - socket OK\n");

        printf("UDP server - tentativo di bind con la porta predefinita... \n");


        //Bind socket to port

        memset(&serveraddr, 0x00, serveraddrlen);           //memset()=imposta servaddr in esadecimale
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_port = htons(SERVPORT);
        serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);     //s_addr=0, cosi da permettere al sistema di connettersi a qualsiasi client
        strcpy(server, SERVER);


        if((bind(sd, (struct sockaddr *)&serveraddr, serveraddrlen)) == -1)    //Si lega la nostra socket a un processo
        {
            perror("UDP server - bind ERROR");
            close(sd);
        }
        else
            printf("UDP server - bind OK\n");

        printf("IP usato %s ,porta server impostata %d\n", inet_ntoa(serveraddr.sin_addr), SERVPORT);
        printf("UDP server - in ascolto...\n");            //keep listening for data


        while(end == 0 && end_ext == 0)
        {
//          pthread_mutex_lock(&lock);

//            if((recvfrom(sd, buffer, buflen, 0, (struct sockaddr *)&clientaddr, &clientaddrlen)) == -1)
//       {
//       perror("UDP Server - ERRORE ricevimento dati");
//       close(sd);
//       }
//           else
//           printf("UDP Server - dati ricevuti...\n");                                  //FASE DI RECEVEFORM MOMENTENEAMENTE SOSPESA

//       printf("UDP Server ha ricevuto il seguente messaggio: \"%s\" \n", bufptr);      //DA USARE IN SEGUITO PER SCEGLIERE IL FRAME FILTRATO
//       printf("dalla porta %d e indirizzo %s.\n", ntohs(clientaddr.sin_port),          //DI INTERESSE DALL'IMAGE PROCESSING (switch)
//       inet_ntoa(clientaddr.sin_addr));

//          pthread_mutex_unlock(&lock);

            switch(buffer[0]){

            case 02:
                thresh1=buffer[1];
                break;

            case 03:
                thresh2=buffer[1];
                break;

               // case

            }


            //Risposta al client

//            pthread_mutex_lock(&lock);

//            if((sendto(sd, bufptr, buflen, 0, (struct sockaddr *)&clientaddr, clientaddrlen)) == -1)
//            {
//                perror("UDP server - ERRORE di risposta");
//                close(sd);
//            }

//            pthread_mutex_unlock(&lock);

        }

    }

}
