
/********Filtraggio dell'immagine acquisita********/
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>       //headers files dell'IMAGE PROCESSING
#include <opencv2/imgproc/imgproc.hpp>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>       //pthread_create-join-exit
#include <string.h>        //memset
#include <sys/types.h>     //header files per leggere le API socket
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//
#include <sstream>
#include <iostream>
#define PI 3.14159265
using namespace std;
using namespace cv;

extern int channel;
extern int thresh1;
extern int thresh2;
extern int pixeld;
extern int pixele;
extern int thcan;
extern int value1;
extern int value2;
extern int length;
extern int gap;
extern int angle;

extern char buffer[100];

int buffer1[(2560*2)-1];
int buffer2[(2560*2)-1];
int buffer3[256];
int buffer4[(2560*2)-1];
int buffer5[(2560*2)-1];
int tmp_sum=0;
bool adaptive_threshold_init=true;

extern pthread_mutex_t lock;

//extern int numero_immagine;

void initAdaptativeThreshold(double S,double T,double S1, double T1){

    for (int i=0;i<(2560*2)-1;i++){
        buffer1[i]=(unsigned int)((float)(i-2550)/(S));
        buffer2[i]=(unsigned int)((float)(i)*(1-T)/2);

        buffer4[i]=(unsigned int)((float)(i-2550)/(S1));
        buffer5[i]=(unsigned int)((float)(i)*(1+T1));
    }
    for (int i=0;i<256;i++){
        buffer3[i]=tmp_sum;
        tmp_sum+=10;
    }
}


void adaptativeThreshold(Mat &input, Mat &output){

    output.create(input.size(),input.type());

    int nrow=input.rows;
    int ncol=input.cols;

    unsigned int pn=0;
    unsigned int gn=0;
    unsigned int hn=0;
    unsigned int gnp[ncol];
    unsigned int reverse=0;

    unsigned int pp=0;

    for(int row=0;row<nrow;row++){
        unsigned char* inp=input.ptr<unsigned char>(row);
        unsigned char* oup=output.ptr<unsigned char>(row);
        if(reverse==1){
            reverse=0;
            for (int col=ncol-1;col>=0;col--){
                pn=buffer3[inp[col]];

                gn-=buffer1[2550+gn-pn];
                hn=(gn+gnp[col]);
                gnp[col]=gn;

                //if (pn<buffer2[hn] || pn<buffer5[pp]){
                if (pn<buffer2[hn]){
                    oup[col]=0;
                    pp-=buffer4[2550+pp-pn];
                }
                else
                    oup[col]=255;
            }
        }
        else{
            reverse=1;
            for (int col=0;col<ncol;col++){
                pn=buffer3[inp[col]];

                if (row==0){
                    gn-=buffer1[2550+gn-pn];
                    hn=gn;
                    gnp[col]=gn;
                }
                else{
                    gn-=buffer1[2550+gn-pn];
                    hn=(gn+gnp[col]);
                    gnp[col]=gn;
                }

                //if (pn<buffer2[hn] || pn<buffer5[pp]){
                if (pn<buffer2[hn]){
                    oup[col]=0;
                    pp-=buffer4[2550+pp-pn];
                }
                else
                    oup[col]=255;
            }
        }
    }
}


void Erosion(Mat &input, Mat &output, int erosion_elem, int erosion_size){
  int erosion_type;
  if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
  else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
  else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }

  Mat element = getStructuringElement( erosion_type,
                                       Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                       Point( erosion_size, erosion_size ) );

  // Apply the erosion operation
  erode( input, output, element );
}


void Dilation(Mat &input, Mat &output, int dilation_elem, int dilation_size){
  int dilation_type;
  if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
  else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
  else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

  Mat element = getStructuringElement( dilation_type,
                                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                       Point( dilation_size, dilation_size ) );
  // Apply the dilation operation
  dilate( input, output, element );
}


void lineExtraction(Mat &input, Mat &output,int thresh, int length, int gap,
                    int angle, int middle, int h_roi){
        int dist_sx, dist_dx;
        float atan;
        vector<Vec4i> lines;
        Point mid = Point(middle , h_roi-2); //punto al centro del bordo inferiore del frame

        cvtColor(input, output, CV_GRAY2BGR);
        //la trasformata di Hough mi estrae le linee nel vettore lines;
        //in realtà ogni elemento di lines contiene le coordinate dei
        //due punti attraversati dalla linea rilevata
        HoughLinesP(input, lines, 1, CV_PI/180, thresh, length, gap );

        for( size_t j = 0; j < lines.size(); j++ )
        {
               Vec4i l = lines[j];
               double deltax = l[2]-l[0];
               double deltay = l[3]-l[1];
               Point pt0 = Point(l[0], l[1]);
               Point pt1 = Point(l[2], l[3]);

               if(deltax != 0)
               {
                  //grazie alle coordinate dei punti posso calcolare
                  //il rapporto incrementale della linea e quindi l'arctan
                  atan = abs(deltay/deltax) * 180 / PI;
                  //se l'arctan è più grande di una certa soglia allora
                  //la linea viene tracciata
                  if(atan > angle){
                     line( output, pt0, pt1, Scalar(0,0,255), 3, CV_AA);
                     ellipse(output,mid,Size(3,3),0,0,360,Scalar( 255, 0, 0 ),2);
                     //se vi sono dei punti alla stessa altezza del punto centrale
                     //allora viene calcolata la distanza tra i due, questo perché
                     //calcoliamo la distanza geometri delle linee dal centro;
                     //viene anche tracciata una linea a solo scopo visivo
                     if(pt0.y == h_roi-2){
                         line( output, mid, pt0, Scalar(0,255,255), 3, CV_AA);
                         ellipse(output,pt0,Size(3,3),0,0,360,Scalar( 255, 0, 0 ),2);
                         dist_sx = mid.x - pt0.x;
                         if(dist_sx > 0) cout << "Sinistra: " << dist_sx << "\n";
                     }
                     if(pt1.y == h_roi-2){
                         line( output, mid, pt1, Scalar(0,255,0), 3, CV_AA);
                         ellipse(output,pt1,Size(3,3),0,0,360,Scalar( 255, 0, 0 ),2);
                         dist_dx = pt1.x - mid.x;
                         if(dist_dx > 0) cout << "Destra: " << dist_dx << "\n";
                     }
                  }
               }
        }
}


void bgrHsvGray(Mat &input, Mat &output, int channel){
        Mat hsv, gray;
        cvtColor(input, hsv, COLOR_BGR2HSV); //conversione in HSV
        cvtColor(hsv, gray, COLOR_BGR2GRAY); //mi preparo un matrice singolo canale
        output = gray;
        for(int y=0;y<hsv.rows;y++)
        {
            for(int x=0;x<hsv.cols;x++)
             {
                // acquisisco il valore del pixel
                Vec3b intensity = hsv.at<Vec3b>(Point(x,y));
                // inserisco in output i valori del canale desiderato(0-Hue, 1-Sat, 2-Val)
                output.at<uchar>(Point(x,y)) = intensity.val[channel];
             }
        }
}


void SobelDeriv(Mat &input, Mat &output,int ker){
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    /// Gradient X
    Sobel( input, grad_x, ddepth, 1, 0, ker, scale, delta, BORDER_DEFAULT );
    /// Gradient Y
    Sobel( input, grad_y, ddepth, 0, 1, ker, scale, delta, BORDER_DEFAULT );

    convertScaleAbs( grad_x, abs_grad_x );
    convertScaleAbs( grad_y, abs_grad_y );
    addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, output );
}


int kernel(int value){
    //visto che le trackbar non sono personalizzabili sui valore numerici
    //ho creato questa funzione per fare in modo che la trackbar restituisca solo
    //valori dispari in questo modo è possibile variare il kernel delle funzioni
    //che lo richiedono
    int ker;
    if(value == 0) ker = 1;
    if(value == 1) ker = 3;
    if(value == 2) ker = 5;
    if(value == 3) ker = 7;
    if(value == 4) ker = 9;
    if(value == 5) ker = 11;
    if(value == 6) ker = 13;
    if(value == 7) ker = 15;
    return ker;
}


void* ImageProcessing_function(void*img)
{

    Mat frame, gray, sob, blur, roi, th, canny, dil, erod, final;
    int n_frame,w_frame,h_frame, i, ker1, ker2;
//    int channel = 0;
//    int thresh1 = 30;
//    int thresh2 = 50;
//    int pixeld = 1;
//    int pixele = 1;
//    int thcan = 50;
//    int value1 = 3;
//    int value2 = 1;
//    int length = 40;
//    int gap = 5;
//    int angle =60;

    char *bufptr=buffer;
    bufptr=(char*) img;

    VideoCapture vid("/home/alessio/Scrivania/Video tirocinio/run0.avi");
    if ( !vid.isOpened() )
    {
         cout << "Errore nell'apertura del video" << endl;
         return NULL;
    }
    n_frame = vid.get(CV_CAP_PROP_FRAME_COUNT);
    w_frame = vid.get(CV_CAP_PROP_FRAME_WIDTH);
    h_frame = vid.get(CV_CAP_PROP_FRAME_HEIGHT);
    cout << "Numero frame video: " << n_frame << endl;
    cout << "Larghezza frame video: " << w_frame << endl;
    cout << "Altezza frame video: " << h_frame << endl;

    //Inizializzazione parametri filtro CUSTOM
    double S=w_frame/2;
    double T=0.35;
    double S1=w_frame/16;
    double T1=0.5;
    initAdaptativeThreshold(S,T,S1,T1);

    for(i=0;i<n_frame;i++){
        bool bSuccess = vid.read(frame); //acquisizione frame dal video
        if (!bSuccess)
        {
                        cout << "Errore nell'acquisizione del frame" << endl;
                        return NULL;
        }

        namedWindow( "Source", CV_WINDOW_AUTOSIZE );
        imshow( "Source", frame );

        //Inizializzazione delle trackbar
        //Selezionare solo le trackbar dei filtri utilizzati

        namedWindow( "Threshold", CV_WINDOW_AUTOSIZE );
        createTrackbar( "Threshold: ", "Threshold", &thresh1, 255);

        //namedWindow( "HSV", CV_WINDOW_AUTOSIZE );
        //createTrackbar( "Channel: ", "HSV", &channel, 2);
        //namedWindow( "Dilation", CV_WINDOW_AUTOSIZE );
        //createTrackbar( "Pixel: ", "Dilation", &pixeld, 10);
        //namedWindow( "Erosion", CV_WINDOW_AUTOSIZE );
        //createTrackbar( "Pixel: ", "Erosion", &pixele, 10);
        //namedWindow( "Canny", CV_WINDOW_AUTOSIZE );
        //createTrackbar( "Threshold: ", "Canny", &thcan, 100);
        //namedWindow( "Blur", CV_WINDOW_AUTOSIZE );
        //createTrackbar( "Kernel: ", "Blur", &value1, 7);
        //ker1= kernel(value1);
        //namedWindow( "Sobel", CV_WINDOW_AUTOSIZE );
        //createTrackbar( "Kernel: ", "Sobel", &value2, 3);
        //ker2= kernel(value2);
        //namedWindow( "Lines", CV_WINDOW_AUTOSIZE );
        //createTrackbar( "Threshold: ", "Lines", &thresh2, 255);
        //createTrackbar( "minLinLenght: ", "Lines", &length, 255);
        //createTrackbar( "maxLineGap: ", "Lines", &gap, 255);
        //createTrackbar( "Angle: ", "Lines", &angle, 180);

        //Applicazione dei filtri al frame acquisito
        //Selezionare i filtri desiderati
        //cvtColor(frame, gray, COLOR_BGR2GRAY);
        //bgrHsvGray(frame,gray,channel); // channel: 0-Hue, 1-Sat, 2-Val
        //GaussianBlur(gray,blur,Size(ker1,ker1),0);
        //SobelDeriv(blur, sob, ker2);
        //Selezionare il ROI adatto al video che si vuole processare
        //roi = sob(Rect(0,120,352,120)); //1Giorno,2Notte
        //roi = sob(Rect(0, 260, 752, 220)); //run0,run1
        //roi = sob(Rect(0, 160, 640, 200)); //640giorno,640giorno2,640corsa
        //roi = sob(Rect(0, 260, 640, 220)); //sbt1,sbt2(tras)
        threshold(roi,th,thresh1,255,0); //thresholding binario
        threshold(roi, th, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU); //otsu
        adaptiveThreshold(roi, th, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 3, -1);
        adaptativeThreshold(roi, th); //thresholding custom
        Canny(th, canny, thcan, thcan*3, 3);
        Erosion(th,erod, 2, pixele);
        Dilation(erod,dil, 2, pixeld);
        //importante: nell'ultimo valore della funzione va inserita l'altezza della ROI
        //lineExtraction(dil, final, thresh2, length, gap, angle, w_frame/2, 220);

        //Visualizzazione filtri
        //Selezionare solo le finestre dei filtri utilizzati
        imshow( "Source", frame );
        //imshow( "HSV", gray );
        //imshow( "Gray", gray );
        //imshow( "Blur", blur );
        //imshow( "Sobel", sob );
        //imshow( "ROI", roi );
        imshow( "Threshold", th );
        //imshow( "Canny", canny );
//        imshow( "Dilation", dil );
//        imshow( "Erosion", erod );
//        imshow("Lines", final);
        waitKey(100);

        pthread_mutex_lock(&lock);                  //viene bloccato bufptr per uso esclusivo di ImageProcessing_function



        unsigned char *frame_buffer=th.data;     //conversione di una variabile tipo Mat in char (per inserire il frame nel buffer)
        int buflen = th.rows * th.cols;
        memcpy(bufptr,frame_buffer,buflen);        //bufptr indirizzo dove copiare i dati contenuti in frame_buffer


        pthread_mutex_unlock(&lock);

    }

  return 0;

}
