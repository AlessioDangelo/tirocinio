﻿
//STEP 1. pulizia del codice lato udp v
//STEP 2. introduzione del doppio while per gestire la riconnessione v
//STEP 3. creazione del main per avviare i due thread di UDP e di processamento delle immagini v
//STEP 4. compilare il tutto v
//-------------------------------------------

//STEP 1. implementazione comandi lato client
//STEP 2. invio di frame che sono di dimensione superiore a 64k che è il limite di UDP


/********Creazione e terminazione thread********/

#include <unistd.h>        //close
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>       //pthread_create-join-exit, lock-unlock
#include <string.h>        //memset
#include <sys/types.h>     //header files per leggere le API socket
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
//
#include <sstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>       //headers files dell'IMAGE PROCESSING
#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/videoio.hpp>
#define PI 3.14159265
#include <stdlib.h>
//
#define SERVPORT 3000             //Porta in ascolto su cui arrivano i dati
#define SERVER "127.0.0.1"

int channel = 0;
int thresh1 = 30;
int thresh2 = 50;
int pixeld = 1;
int pixele = 1;
int thcan = 50;
int value1 = 3;
int value2 = 1;
int length = 40;
int gap = 5;
int angle =60;

char buffer[100];

extern void *ImageProcessing_function(void*img);
extern void *UDPserver_function(void *srv);


pthread_mutex_t lock;             //Costrutto si sincronizzazione per l'accesso al buffer condiviso


int end_ext = 0;


int main(int argc, char*argv[])
{

    pthread_t thread1,thread2;
    char *contenuto_buffer;
    int  thr1,thr2;


    if(pthread_mutex_init(&lock,NULL) != 0)
    {
        printf("\nMutex fallito\n");
        return 1;
    }


    thr1 = pthread_create( &thread1, NULL, UDPserver_function, (void*) contenuto_buffer);
    if(thr1)
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",thr1);
        exit(EXIT_FAILURE);
    }


    thr2 = pthread_create( &thread2, NULL, ImageProcessing_function, (void*) contenuto_buffer);
    if(thr2)
    {
        fprintf(stderr,"Error - pthread_create() return code: %d\n",thr2);
        exit(EXIT_FAILURE);
    }



    //printf("pthread_create() per la UDP_function ritorna: %d\n",thr1);
    //printf("pthread_create() per l'ImageProcessing_function ritorna: %d\n",thr2);


    //pthread_join( thread3, NULL);                  //sospensione del thread corrente, fino alla
    pthread_join( thread1, NULL);                  //terminazione del thread 1 (nel primo caso) e del thread 2 (nel secondo)
    pthread_join( thread2, NULL);

    pthread_mutex_destroy(&lock);

    exit(EXIT_SUCCESS);

}




