

/*****************udpserver*****************/

#include <QCoreApplication>
#include <unistd.h>
#include <stdio.h>            /*Header necessari per andare a leggere le API dei socket*/
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>        /*Header necessari per la creazione-chiusura del socket*/
#include <sys/socket.h>       /*sia per la parte di creazione ,che in quella di assegnamento dell'ip ,che in quella di scambio dati*/
#include <netinet/in.h>       /*Necessaria per AF_INET nel socket*/
#include <arpa/inet.h>

#define SERVPORT 3333         /*Porta del server in ascolto*/


int main(int argc, char *argv[])
{
int sd, rc;                                  /*Definizione di variabili e delle due strutture usati*/
struct sockaddr_in serveraddr, clientaddr;
int clientaddrlen = sizeof(clientaddr);
int serveraddrlen = sizeof(serveraddr);
char buffer[100];
char *bufptr = buffer;
int buflen = sizeof(buffer);

/*AFI_INET rappresenta il dominio del nostro socket,dato da un indirizzo IP (host) e da un numero di porta dell'host*/
/*SOCK_DGRAM rappresenta la linea di trasporto dati scelta su protocollo UDP*/

if((sd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)      /*sd socket in ascolto*/
{
perror("UDP server ERRORE socket");
exit(-1);
}
else
printf("UDP server - socket ATTIVO\n");

printf("UDP server - tentativo di bind con la porta predefinita... \n");


/*******Inizio fase di bind(), legare il socket ad un indirizzo IP*******/

memset(&serveraddr, 0x00, serveraddrlen);           /*memset() è una funzione che ci permette di settare la variabile servaddr in esadecimale*/
serveraddr.sin_family = AF_INET;                    /*Si imposta dominio e porta per il nostro server. Il dominio è stato impostato prima*/
serveraddr.sin_port = htons(SERVPORT);              /*alla creazione del socket, mentre la porta è quella definita all'inizio con il #define*/
serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);     /*s_addr sarà impostata a 0, in modo da permettere al sistema di connetersi a qualsiasi*/
                                            /*client che usa la porta server predefinita*/

if((rc = bind(sd, (struct sockaddr *)&serveraddr, serveraddrlen)) < 0)
{
perror("UDP server - ERRORE bind");
close(sd);
exit(-1);
}
else
printf("UDP server - bind ESEGUITA\n");

printf("IP usato %s ,porta server impostata %d\n", inet_ntoa(serveraddr.sin_addr), SERVPORT);
printf("UDP server - in ascolto...\n");


/*******Inizio fase di ricevimento dati dal client, recvform()*******/

/*recvform() può attendere dati dal client per un tempo indefinito*/

if((rc = recvfrom(sd, bufptr, buflen, 0, (struct sockaddr *)&clientaddr, clientaddrlen)) < 0)
{
perror("UDP Server - ERRORE ricevimento dati");
close(sd);
exit(-1);
}
else
printf("UDP Server - dati ricevuti...\n");

printf("UDP Server ha ricevuto il seguente messaggio: \"%s\" \n", bufptr);
printf("dalla porta %d e indirizzo %s.\n", ntohs(clientaddr.sin_port),
inet_ntoa(clientaddr.sin_addr));


/*******Fase di risposta al client, sendto()*******/

printf("UDP Server risponde al UDP client...\n");
rc = sendto(sd, bufptr, buflen, 0, (struct sockaddr *)&clientaddr, clientaddrlen);
if(rc < 0)
{
perror("UDP server - ERRORE di risposta");
close(sd);
exit(-1);
}
else
printf("UDP Server - risposta effettuata...\n");

/*Quando i dati saranno inviati, chiudiamo il socket, close()*/

close(sd);
exit(0);
}
